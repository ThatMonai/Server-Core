package net.thatmonai.community.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Bukkit;

import net.thatmonai.community.Community;

public class MySQLAPI {

    public static String host;
    public static String port;
    public static String user;
    public static String database;
    public static String password;
    public static Connection con;

    public static void connect() {
         if(!isConnected()) {
             try {
                con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
                Bukkit.getConsoleSender().sendMessage(Community.prefix + "Die MySQL-Verbindung wurde aufgebaut!");
            } catch (SQLException e) {
		e.printStackTrace();
            }
	}
    }

    public static void disconnect() {
        if(isConnected()) {
	
            try {
                con.close();
                Bukkit.getConsoleSender().sendMessage(Community.prefix + "Die MySQL-Verbindung wurde geschlossen!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
    	}
    }
	
    public static Boolean isConnected() {
        
        return (con == null ? false : true);
	
    }
	
    public static Connection getConnection() {

        return con;
	
    }

    public static boolean isUserExists(UUID uuid) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("SELECT Spielername FROM spielerliste WHERE UUID = ?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();
            
            return rs.next();
	} catch (SQLException e) {
            e.printStackTrace();
	}
            return false;
    }	
}

