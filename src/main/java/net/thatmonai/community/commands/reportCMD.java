package net.thatmonai.community.commands;

import net.thatmonai.community.Community;
import net.thatmonai.community.managers.ReportManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Luca on 30.06.2017.
 */
public class reportCMD implements CommandExecutor {

    public static String reason1 = "Flying";
    public static String reason2 = "Killaura";
    public static String reason3 = "Autoclicker";
    public static String reason4 = "Beleidigung";
    public static String reason5 = "Spamming";
    public static String reason6 = "Bugusing";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;
            if (cmd.getName().equalsIgnoreCase("report")) {

                if (args.length == 0) {

                    player.sendMessage(Community.prefix + "§8---[§cReport-Reasons§8]---");
                    player.sendMessage("§c1 §8| §c" + reason1);
                    player.sendMessage("§c2 §8| §c" + reason2);
                    player.sendMessage("§c3 §8| §c" + reason3);
                    player.sendMessage("§c4 §8| §c" + reason4);
                    player.sendMessage("§c5 §8| §c" + reason5);
                    player.sendMessage("§c6 §8| §c" + reason6);

                } else if (args.length == 1) {

                    Player target = Bukkit.getPlayer(args[0]);

                    if (target != null) {

                        player.sendMessage(Community.prefix + "§7Bitte gebe einen Grund an, um §c" + target.getName() + " §7zu reporten!");
                        player.sendMessage(Community.prefix + "§8---[§cReport-Reasons§8]---");
                        player.sendMessage("§c1 §8| §c" + reason1);
                        player.sendMessage("§c2 §8| §c" + reason2);
                        player.sendMessage("§c3 §8| §c" + reason3);
                        player.sendMessage("§c4 §8| §c" + reason4);
                        player.sendMessage("§c5 §8| §c" + reason5);
                        player.sendMessage("§c6 §8| §c" + reason6);

                    } else {
                        player.sendMessage(Community.prefix + "§c" + target + " ist nicht Online!");
                    }

                } else if (args.length == 2) {

                    Player target = Bukkit.getPlayer(args[0]);

                    if (!args[0].equalsIgnoreCase(player.getName())) {

                        if (target != null) {

                            if (args[1].equalsIgnoreCase("1")) {

                                ReportManager.onReport(player, target, reason1);

                            } else if (args[1].equalsIgnoreCase("2")) {

                                ReportManager.onReport(player, target, reason2);

                            } else if (args[1].equalsIgnoreCase("3")) {

                                ReportManager.onReport(player, target, reason3);

                            } else if (args[1].equalsIgnoreCase("4")) {

                                ReportManager.onReport(player, target, reason4);

                            } else if (args[1].equalsIgnoreCase("5")) {

                                ReportManager.onReport(player, target, reason5);

                            } else if (args[1].equalsIgnoreCase("6")) {

                                ReportManager.onReport(player, target, reason6);

                            } else {
                                player.sendMessage(Community.syntax + "/report <Spieler> <Zahl>");
                            }

                        } else {
                            player.sendMessage(Community.prefix + "§c" + target + " ist nicht Online!");
                        }
                    } else {
                        player.sendMessage(Community.prefix + "§cDu kannst dich selbst nicht reporten!");
                    }
                }

            } else {
                player.sendMessage(Community.syntax + "/report <Spieler> <Zahl>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }
}
