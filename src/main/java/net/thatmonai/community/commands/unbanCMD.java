package net.thatmonai.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.community.Community;
import net.thatmonai.community.managers.BanManager;

public class unbanCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("unban")) {

                if (player.hasPermission("server.unban")) {

                    if (args.length == 1) {

                        if (BanManager.isUserExists(args[0]) == true) {

                            BanManager.setUnBan(args[0]);
                            player.sendMessage(Community.prefix + "Der Spieler §c" + args[0] + " §7wurde entbannt!");

                        } else {
                            player.sendMessage(Community.prefix + "Dieser Spieler existiert nicht, oder ist nicht gebannt!");
                        }

                    } else {
                        player.sendMessage(Community.syntax + "/unban <Spieler>");
                    }

                } else {
                    player.sendMessage(Community.noperms);
                }

            } else {
                player.sendMessage(Community.syntax + "/unban <Spieler>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }

}
