package net.thatmonai.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.community.Community;

public class kickCMD implements CommandExecutor {

    public static String g1 = "§cHacking | Live";
    public static String g2 = "§cBeleidigung";
    public static String g3 = "§cSpamming";

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("kick")) {

                if (player.hasPermission("server.kick")) {

                    if (args.length == 0) {

                        player.sendMessage(Community.prefix + "§8---[§cKick-Reasons§8]---");
                        player.sendMessage("§c1 §8| §cHacking | Live");
                        player.sendMessage("§c2 §8| §cBeleidigung");
                        player.sendMessage("§c3 §8| §cSpamming");

                    } else if (args.length == 1) {

                        Player target = Bukkit.getPlayer(args[0]);

                        if (target != null) {

                            player.sendMessage(Community.prefix + "§7Bitte gebe einen Grund an, um §c" + target.getName() + " §7vom Server zu kicken!");
                            player.sendMessage(Community.prefix + "§8---[§cKick-Reasons§8]---");
                            player.sendMessage("§c1 §8| §cHacking | Live");
                            player.sendMessage("§c2 §8| §cBeleidigung");
                            player.sendMessage("§c3 §8| §cSpamming");

                        } else {
                            player.sendMessage(Community.prefix + "§c" + target + " ist nicht Online!");
                        }

                    } else if (args.length == 2) {

                        Player target = Bukkit.getPlayer(args[0]);

                        if (target != null) {

                            if (args[1].equalsIgnoreCase("1")) {

                                kickMessage(player, g1, target);

                            } else if (args[1].equalsIgnoreCase("2")) {

                                kickMessage(player, g2, target);

                            } else if (args[1].equalsIgnoreCase("3")) {

                                kickMessage(player, g3, target);

                            } else {
                                player.sendMessage(Community.syntax + "/kick <Spieler> <Zahl>");
                            }

                        } else {
                            player.sendMessage(Community.prefix + "§c" + target + " ist nicht Online!");
                        }

                    } else {
                        player.sendMessage(Community.syntax + "/syntax");
                    }

                } else {
                    player.sendMessage(Community.noperms);
                }

            } else {
                player.sendMessage(Community.syntax + "/kick <Spieler> <Zahl>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }

    public void kickMessage(Player player, String message, Player target) {

        target.kickPlayer("§7Du wurdest vom §cThatMonai.NET Community-Server §7gekickt! \n\n §7Grund: §c" + message);

        player.sendMessage("§8");
        player.sendMessage(Community.prefix + "§7Der Spieler §c" + target.getName() + "§7 wurde vom Server gekickt!");
        player.sendMessage("§8");
    }

}
