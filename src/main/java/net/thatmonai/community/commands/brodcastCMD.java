package net.thatmonai.community.commands;

import net.thatmonai.community.Community;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class brodcastCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("brodcast")) {

                if (player.hasPermission("server.brodcast")) {

                    if (args.length > 0) {

                        String message = "";

                        for (int i = 0; i < args.length; i++) {

                            message = message + args[i] + " ";

                        }

                        message = ChatColor.translateAlternateColorCodes('&', message);

                        Bukkit.broadcastMessage(Community.prefix + message);

                    } else {
                        player.sendMessage(Community.syntax + "/brodcast <Nachricht>");
                    }

                } else {
                    player.sendMessage(Community.noperms);
                }

            } else {
                player.sendMessage(Community.syntax + "/brodcast <Nachricht>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }

}
