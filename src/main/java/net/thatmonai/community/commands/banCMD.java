package net.thatmonai.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.community.Community;
import net.thatmonai.community.managers.BanManager;

public class banCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("ban")) {

                if (player.hasPermission("server.ban")) {

                    if (args.length == 0) {

                        player.sendMessage(Community.prefix + "§8---[§cBan-Reasons§8]---");
                        player.sendMessage("§c1 §8| §c" + BanManager.reason1);
                        player.sendMessage("§c2 §8| §c" + BanManager.reason2);
                        player.sendMessage("§c3 §8| §c" + BanManager.reason3);
                        player.sendMessage("§c4 §8| §c" + BanManager.reason4);
                        player.sendMessage("§c5 §8| §c" + BanManager.reason5);
                        player.sendMessage("§c5 §8| §c" + BanManager.reason6);

                    } else if (args.length == 1) {
                        
                        player.sendMessage(Community.prefix + "§8---[§cBan-Reasons§8]---");
                        player.sendMessage("§c1 §8| §c" + BanManager.reason1);
                        player.sendMessage("§c2 §8| §c" + BanManager.reason2);
                        player.sendMessage("§c3 §8| §c" + BanManager.reason3);
                        player.sendMessage("§c4 §8| §c" + BanManager.reason4);
                        player.sendMessage("§c5 §8| §c" + BanManager.reason5);
                        player.sendMessage("§c6 §8| §c" + BanManager.reason6);

                    } else if (args.length == 2) {

                        Player target = Bukkit.getPlayer(args[0]);

                        if (args[1].equalsIgnoreCase("1")) {

                            BanPlayer(target, args[0], player, BanManager.reason1);

                        } else if (args[1].equalsIgnoreCase("2")) {

                            BanPlayer(target, args[0], player, BanManager.reason2);

                        } else if (args[1].equalsIgnoreCase("3")) {

                            BanPlayer(target, args[0], player, BanManager.reason3);

                        } else if (args[1].equalsIgnoreCase("4")) {

                            BanPlayer(target, args[0], player, BanManager.reason4);

                        } else if (args[1].equalsIgnoreCase("5")) {

                            BanPlayer(target, args[0], player, BanManager.reason5);

                        } else {
                            player.sendMessage(Community.syntax + "/ban <Spieler> <Zahl>");
                        }

                    }

                } else {
                    player.sendMessage(Community.noperms);
                }

            } else {
                player.sendMessage(Community.syntax + "/ban <Spieler> <Zahl>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }

    public void BanPlayer(Player target2, String target, Player sender, String Reason) {

        if (BanManager.isUserExists(target) == true) {

            BanManager.setBan(target, true, Reason);

            if (target2 != null) {

                target2.kickPlayer(Community.banMessage + Reason);

            }

            Bukkit.getOnlinePlayers().forEach(players -> {

                if (players.hasPermission("server.ban")) {

                    players.sendMessage("§8");
                    players.sendMessage(Community.prefix + "Der Spieler §c" + target + " §7wurde von §c" + sender.getName() + " §7vom Server gebannt!");
                    players.sendMessage(Community.prefix + "Grund: §c" + Reason);
                    players.sendMessage("§8");

                }

            });
        } else {
            sender.sendMessage(Community.prefix + "Der Spieler §c" + target + " §7ist nicht in der Datenbank vorhanden!");
        }

    }

}
