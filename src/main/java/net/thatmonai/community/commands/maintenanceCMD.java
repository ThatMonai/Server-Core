package net.thatmonai.community.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.thatmonai.community.Community;
import net.thatmonai.community.managers.ConfigManager;

public class maintenanceCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase("maintenance")) {

                if (player.hasPermission("server.maintenance")) {

                    if (args.length == 1) {

                        if (args[0].equalsIgnoreCase("on")) {

                            ConfigManager.setMaintenance(true);
                            player.sendMessage(Community.prefix + "§cDu hast den Wartungsmodus aktiviert! Es können nur Team-Mitglieder den Server betreten!");

                            Bukkit.getOnlinePlayers().forEach(target -> {

                                if (!target.hasPermission("server.maintenance")) {

                                    target.kickPlayer(Community.kickMSG + "§cWartungsarbeiten \n\n§7Für mehrere Informationen schaue auf Twitter nacht! \n§b@ThatMonai");

                                }

                            });

                        } else if (args[0].equalsIgnoreCase("off")) {

                            ConfigManager.setMaintenance(false);
                            player.sendMessage(Community.prefix + "§cDu hast den Wartungsmodus deaktiviert! Es können nun alle Spieler wieder joinen!!");

                        }

                    } else {
                        player.sendMessage(Community.syntax + "/maintenance <on/off>");
                    }

                } else {
                    player.sendMessage(Community.noperms);
                }

            } else {
                player.sendMessage(Community.syntax + "/maintenance <on/off>");
            }

        } else {
            Bukkit.getConsoleSender().sendMessage(Community.noplayer);
        }

        return false;
    }

}
