package net.thatmonai.community.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.thatmonai.community.managers.GroupManager;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();

        if (player.hasPermission(GroupManager.admin_perm)) {

            event.setJoinMessage("§8[§a+§8] §4" + player.getName());
            player.sendMessage("§7Willkommen §4" + player.getName() + "§7, auf unserem Minecraft-Server!");

        } else if (player.hasPermission(GroupManager.mod_perm)) {

            event.setJoinMessage("§8[§a+§8] §c" + player.getName());
            player.sendMessage("§7Willkommen §c" + player.getName() + "�7, auf unserem Minecraft-Server!");

        } else if (player.hasPermission(GroupManager.builder_perm)) {

            event.setJoinMessage("§8[§a+§8] §b" + player.getName());
            player.sendMessage("§7Willkommen §b" + player.getName() + "§7, auf unserem Minecraft-Server!");

        } else if (player.hasPermission(GroupManager.vip_perm)) {

            event.setJoinMessage("§8[§a+§8] §5" + player.getName());
            player.sendMessage("§7Willkommen §5" + player.getName() + "§7, auf unserem Minecraft-Server!");

        } else if (player.hasPermission(GroupManager.gold_perm)) {

            event.setJoinMessage("§8[§a+§8] §6" + player.getName());
            player.sendMessage("§7Willkommen §6" + player.getName() + "§7, auf unserem Minecraft-Server!");

        } else if (player.hasPermission(GroupManager.spieler_perm)) {

            event.setJoinMessage("§8[§a+§8] §a" + player.getName());
            player.sendMessage("§7Willkommen §a" + player.getName() + "§7, auf unserem Minecraft-Server!");

        } else {

            event.setJoinMessage(null);
            //KickNoRank(player);

        }

    }

    public static void KickNoRank(Player player) {

        player.kickPlayer("§7Du wurdest vom §cThatMonai.NET Community-Server §7gekickt! \n\n§7Grund: §cKein Rang! (Datenbank Fehler)");

    }

}
