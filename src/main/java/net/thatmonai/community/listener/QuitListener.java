package net.thatmonai.community.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import net.thatmonai.community.managers.GroupManager;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {

        Player player = event.getPlayer();

        if (player.hasPermission(GroupManager.admin_perm)) {

            event.setQuitMessage("§8[§c-§8] §4" + player.getName());

        } else if (player.hasPermission(GroupManager.mod_perm)) {

            event.setQuitMessage("§8[§c-§8] §c" + player.getName());

        } else if (player.hasPermission(GroupManager.builder_perm)) {

            event.setQuitMessage("§8[§c-§8] §b" + player.getName());

        } else if (player.hasPermission(GroupManager.vip_perm)) {

            event.setQuitMessage("§8[§c-§8] §5" + player.getName());

        } else if (player.hasPermission(GroupManager.gold_perm)) {

            event.setQuitMessage("§8[§c-§8] §6" + player.getName());

        } else if (player.hasPermission(GroupManager.spieler_perm)) {

            event.setQuitMessage("§8[§c-§8] §a" + player.getName());

        } else {
            event.setQuitMessage(null);
        }

    }
}
