package net.thatmonai.community.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import net.thatmonai.community.managers.ConfigManager;

public class ServerPingListener implements Listener {

    @EventHandler
    public void onPingServer(ServerListPingEvent event) {

        event.setMaxPlayers(Bukkit.getOnlinePlayers().size() + 1);
        event.setMotd(ConfigManager.getMotd());
    }
}
