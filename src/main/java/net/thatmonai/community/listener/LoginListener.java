package net.thatmonai.community.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import net.thatmonai.community.Community;
import net.thatmonai.community.managers.BanManager;
import net.thatmonai.community.managers.ConfigManager;

public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {

        BanManager.createPlayer(event.getPlayer().getUniqueId(), event.getPlayer());

        if (BanManager.getBan(event.getPlayer().getUniqueId()) == true) {

            event.disallow(null, Community.banMessage + BanManager.getBanReason(event.getPlayer().getUniqueId()));

        } else {
            event.allow();
        }

        if (ConfigManager.getMaintenance() == true) {

            event.disallow(null, Community.maintenanceMSG);

            if (event.getPlayer().hasPermission("server.maintenance")) {

                event.allow();

            }
        }
    }
}
