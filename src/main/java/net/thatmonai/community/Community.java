package net.thatmonai.community;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.thatmonai.community.commands.reportCMD;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.thatmonai.community.commands.banCMD;
import net.thatmonai.community.commands.brodcastCMD;
import net.thatmonai.community.commands.kickCMD;
import net.thatmonai.community.commands.maintenanceCMD;
import net.thatmonai.community.commands.unbanCMD;
import net.thatmonai.community.listener.JoinListener;
import net.thatmonai.community.listener.LoginListener;
import net.thatmonai.community.listener.QuitListener;
import net.thatmonai.community.listener.ServerPingListener;
import net.thatmonai.community.managers.ConfigManager;
import net.thatmonai.community.utils.MySQLAPI;

public class Community extends JavaPlugin {

    public static String prefix = "§8[§aServer§8] §7";
    public static String noplayer = "§cDies ist nur als Spieler möglich!";
    public static String syntax = "§cSyntax: ";
    public static String noperms = "§cDazu hast du keine Rechte!";
    public static String kickNoRankMSG = "§7Du wurdest vom §cThatMonai.net Community-Server §7gekickt! \n\n §7Grund: §cKein Rang! (Datenbank Fehler)";
    public static String banMessage = "§7Du wurdest vom §cThatMonai.net Community-Server §7gebannt! \n\n §7Grund: §c";
    public static String kickMSG = "§7Du wurdest vom §cThatMonai.net Community-Server §7gekickt! \n\n §7Grund: §c";
    public static String maintenanceMSG = "§7Der §cThatMonai.net Community-Server §7befindet sich in Wartungsarbeiten! \n\n §7Für mehrere Informationen schaue doch mal auf Twitter vorbei. \n§b@ThatMonai";

    @Override
    public void onEnable() {

        Bukkit.getConsoleSender().sendMessage(prefix + "Der Server wird gestartet!");

        Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new QuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new LoginListener(), this);
        Bukkit.getPluginManager().registerEvents(new ServerPingListener(), this);

        getCommand("kick").setExecutor(new kickCMD());
        getCommand("ban").setExecutor(new banCMD());
        getCommand("unban").setExecutor(new unbanCMD());
        getCommand("report").setExecutor(new reportCMD());
        getCommand("maintenance").setExecutor(new maintenanceCMD());
        getCommand("brodcast").setExecutor(new brodcastCMD());

        ConfigManager.createMySQL();
        ConfigManager.getMySQL();

        MySQLAPI.connect();

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS spielerliste (ID MEDIUMINT NOT NULL AUTO_INCREMENT,PRIMARY KEY (id) , UUID VARCHAR(100), Spielername VARCHAR(20))");
            PreparedStatement ps2 = MySQLAPI.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS bans (ID MEDIUMINT NOT NULL AUTO_INCREMENT,PRIMARY KEY (id) , UUID VARCHAR(100), Spielername VARCHAR(20), Banned BOOLEAN, Reason VARCHAR(100))");
            ps.executeUpdate();
            ps2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Bukkit.getConsoleSender().sendMessage(prefix + "Der Server wurde gestartet!");

    }

    @Override
    public void onDisable() {

        Bukkit.getConsoleSender().sendMessage(prefix + "Der Server wird geschlossen!");

        Bukkit.getConsoleSender().sendMessage(prefix + "Der Server wurde geschlossen!");

    }

}
