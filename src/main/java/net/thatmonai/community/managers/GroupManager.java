package net.thatmonai.community.managers;


public class GroupManager {
	
    public static String admin_perm = "group.administrator";
    public static String mod_perm = "group.moderator";
    public static String builder_perm = "group.builder";
    public static String vip_perm = "group.vip";
    public static String gold_perm = "group.premium";
    public static String spieler_perm = "group.spieler";
	
}
