package net.thatmonai.community.managers;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

import net.thatmonai.community.utils.MySQLAPI;

public class ConfigManager {

    public static void createMySQL() {

        File ordner = new File("plugins/Community");
        File file = new File("plugins/Community/MySQL.yml");
        File file2 = new File("plugins/Community/Options.yml");

        if (!ordner.exists()) {
            ordner.mkdirs();
        }

        if (!file.exists()) {

            try {
                file.createNewFile();

                YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

                cfg.set("Host", "localhost");
                cfg.set("Port", "3306");
                cfg.set("User", "User");
                cfg.set("Database", "Database");
                cfg.set("Password", "Password");

                try {
                    cfg.save(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!file2.exists()) {
            try {
                file2.createNewFile();

                YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file2);

                cfg.set("maintenance", false);
                cfg.set("motd.normal", "§aThatMonai.net §8| §aCommunity-Server §8[§a1.12§8]\n§bTwitter: §7@ThatMonai");
                cfg.set("motd.maintenance", "§aThatMonai.net §8| §aCommunity-Server §8[§a1.12§8] \\n§8[§cWartungsarbeiten§8] §bTwitter: §7@ThatMonai");

                try {
                    cfg.save(file2);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void getMySQL() {

        File file = new File("plugins/Community/MySQL.yml");

        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        MySQLAPI.host = cfg.getString("Host");
        MySQLAPI.port = cfg.getString("Port");
        MySQLAPI.user = cfg.getString("User");
        MySQLAPI.database = cfg.getString("Database");
        MySQLAPI.password = cfg.getString("Password");

    }

    public static boolean getMaintenance() {

        File file = new File("plugins/Community/Options.yml");

        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        return cfg.getBoolean("maintenance");

    }

    public static String getMotd() {

        File file = new File("plugins/Community/Options.yml");

        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        if (ConfigManager.getMaintenance() == false) {

            return cfg.getString("motd.normal");

        } else {
            return cfg.getString("motd.maintenance");
        }

    }

    public static void setMaintenance(Boolean bool) {

        File file = new File("plugins/Community/Options.yml");
        YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        cfg.set("maintenance", bool);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
