package net.thatmonai.community.managers;

import net.thatmonai.community.Community;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Luca on 30.06.2017.
 */
public class ReportManager {

    public static void onReport(Player reporter, Player target, String reason) {

        reporter.sendMessage(Community.prefix + "Du hast §c" + target.getName() + " §7reportet!");

        Bukkit.getOnlinePlayers().forEach(players -> {

            if (players.hasPermission("server.reportmsg")) {

                players.sendMessage("§8");
                players.sendMessage(Community.prefix + "Der Spieler §c" + target.getName() + " §7wurde von §c" + reporter.getName() + " §7reportet!");
                players.sendMessage(Community.prefix + "Grund: §c" + reason);
                players.sendMessage("§8");

            }

        });

    }

}
