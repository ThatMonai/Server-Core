package net.thatmonai.community.managers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.entity.Player;

import net.thatmonai.community.utils.MySQLAPI;

public class BanManager {

    public static String reason1 = "Hacking | Live";
    public static String reason2 = "Beleidigung";
    public static String reason3 = "Spamming";
    public static String reason4 = "Bugusing";
    public static String reason5 = "Fehlverhalten am Server";
    public static String reason6 = "Sicherheitsban";

    public static void createPlayer(UUID uuid, Player playername) {

        if (!MySQLAPI.isUserExists(uuid)) {

            try {
                PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("INSERT INTO spielerliste (UUID,Spielername) VALUES (?,?)");

                ps.setString(1, uuid.toString());
                ps.setString(2, playername.getName());

                PreparedStatement ps2 = MySQLAPI.getConnection().prepareStatement("INSERT INTO bans (UUID,Spielername,Banned, Reason) VALUES (?,?,?,?)");
                ps2.setString(1, uuid.toString());
                ps2.setString(2, playername.getName());
                ps2.setBoolean(3, false);
                ps2.setString(4, null);

                ps.executeUpdate();
                ps2.executeUpdate();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public static void setBan(String player, Boolean bool, String reason) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("UPDATE bans SET banned = ? , reason = ? WHERE UUID = ?");

            ps.setBoolean(1, bool);
            ps.setString(2, reason);
            ps.setString(3, BanManager.getPlayerUUID(player));

            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void setUnBan(String player) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("UPDATE bans SET banned = ? , reason = ? WHERE Spielername = ?");

            ps.setBoolean(1, false);
            ps.setString(2, null);
            ps.setString(3, player);

            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static boolean getBan(UUID uuid) {
        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("SELECT banned FROM bans WHERE UUID = ?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getBoolean("banned");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null != null;
    }

    public static String getBanReason(UUID uuid) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("SELECT reason FROM bans WHERE UUID = ?");
            ps.setString(1, uuid.toString());
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getString("reason");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isUserExists(String player) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("SELECT UUID FROM spielerliste WHERE Spielername = ?");

            ps.setString(1, player);

            ResultSet rs = ps.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public static String getPlayerUUID(String player) {

        try {
            PreparedStatement ps = MySQLAPI.getConnection().prepareStatement("SELECT UUID FROM bans WHERE Spielername = ?");
            ps.setString(1, player);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                return rs.getString("UUID");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
